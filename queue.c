// Queue source file

#include "queue.h"

void queue_init(){
	first_element = 0;
	last_element = 0;
	// todo first_adr	
}

uint8_t queue_push(char elem, char adr){
	if (last_element+1==QUEUE_SIZE){ // back to zero
		if (first_element==0) return 0; // queue full
		else{
			queue_puff[31]= elem;
			queue_adr[31]= adr;
			last_element = 0;
			return 1; // success
		}
	}
	else{ // regular way
		if (last_element+1==first_element) return 0; // queue full
		else{
			queue_puff[last_element]=elem;
			queue_adr[last_element]=adr;
			last_element++;
			return 1; // success
		}
	}
}

uint8_t queue_pop(){
	if (first_element == last_element) // queue empty
		return 0;
	else{
		first_puff = &queue_puff[first_element];
		first_adr = &queue_adr[first_element];
		if (first_element+1==QUEUE_SIZE)
			first_element = 0;
		else
			first_element++;
		return 1;
	}
}

uint8_t queue_isEmpty(){
	if (first_element==last_element)
		return 1;
	else 
		return 0;
} 