/*
 * wheelmod.c
 * 
 * eeprom
 * 0 - dir
 * 1 - wheel polarity
 * 2 - id
 * 3 - pgain
 * 4 - igain
 * 5 - dgain
 */ 

#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
//#include <string.h>
#include "usb_serial.h"
#include "queue.h"

#define bit_get(p,m) ((p) & (m)) 
#define bit_set(p,m) ((p) |= (m))
#define bit_clear(p,m) ((p) &= ~(m)) 
#define bit_flip(p,m) ((p) ^= (m)) 
#define bit_write(c,p,m) (c ? bit_set(p,m) : bit_clear(p,m)) 
#define BIT(x) (0x01 << (x)) 
#define LONGBIT(x) ((unsigned long)0x00000001 << (x))

#define dev_LM 0
#define dev_PHM (1<<6)
#define ord_LM_sd (21<<2)
#define ord_LM_wl (1<<2)
#define ord_LM_dr (2<<2)
#define ord_LM_pd (3<<2)
#define ord_LM_st (4<<2)
#define ord_LM_gs (5<<2)
#define ord_LM_fs (6<<2)
#define ord_LM_spi (7<<2)
#define ord_LM_s (8<<2)
#define ord_LM_gpi (9<<2)

#define ord_LM_ok (14<<2)
#define ord_LM_error ( 15<<2)

#define ord_LM_setID (16<<2)


//pin definitions
#define LED1 PC4
#define LED2 PC5

int atoi(const char * str);
uint8_t find_next_arg(char *string_to_work);


uint8_t leds_on = 1;

char response[16];

uint8_t phys_to_id[8]; // to get id out of physical adr
uint8_t id_to_phys[8]; // to get physical adr of id

uint8_t spi_idle=1; // 

uint8_t spi_waiting_for_answer[8]; // 8 flags for every 8 slaves


uint8_t select_slave(char *slave_adr);
uint8_t clear_slave(char *slave_adr);
uint16_t spi_communicate();
void spi_handle_response (uint16_t slave_response);


ISR(TIMER0_OVF_vect){
	// device working signal
	bit_flip(PORTC,BIT(LED2));
}
// ISR(SPI_STC_vect){
// 	if (queue_isEmpty()){
// 		spi_idle = 1;
// 		
// 	}
// 	else{
// 		spi_idle = 1;
// 	}
// }

uint8_t find_next_arg(char *string_to_work){
	for (int i=0;string_to_work[i] != '\0';i++){
		if (string_to_work[i] == ';')
			return i;
	}
	return 255;
}


uint16_t spi_communicate(){
	usb_write("sisenesin comi");
	if (queue_pop()) {
		uint16_t par=0;// t�hi parameeter
		select_slave(first_adr);
		SPDR=*first_puff;
		while (!(SPSR&BIT(SPIF))){} // siia hiljem timeout kood
		_delay_us(5); // allow slave to get operate
		par=(SPDR<<8);
		queue_pop();
		SPDR = *first_puff;
		while (!(SPSR&BIT(SPIF))){} // siia hiljem timeout kood
		par|=SPDR;
		clear_slave(first_adr);		
		return par;
	}
	else 
		usb_write("midagi l�ks untsu");
		return 0;
}

uint8_t clear_slave(char *slave_adr){
	if (*slave_adr==1){
		bit_set(PORTB,BIT(PB4));
		return 1; // operation successful
	}
	else if (*slave_adr==2){
		bit_set(PORTB,BIT(PB0));
		return 1;
	}
	else if (*slave_adr==3){
		bit_set(PORTC,BIT(PC2));
		return 1;
	}
	else if (*slave_adr==4){
		bit_set(PORTD,BIT(PD5));
		return 1;
	}
	else if (*slave_adr==5){
		bit_set(PORTB,BIT(PB6));
		return 1;
	}
	else if (*slave_adr==6){
		bit_set(PORTB,BIT(PB7));
		return 1;
	}
	else if (*slave_adr==7){
		bit_set(PORTC,BIT(PC7));
		return 1;
	}
	else if (*slave_adr==8){
		bit_set(PORTC,BIT(PC6));
		return 1;
	}
	// add if for every SS pin
	else 
		return 0; // adr not found	
}

uint8_t select_slave(char *slave_adr){
	if (*slave_adr==1){
		bit_clear(PORTB,BIT(PB4));
		return 1; // operation successful
	}
	else if (*slave_adr==2){
		bit_clear(PORTB,BIT(PB0));
		return 1; // operation successful
	}
	else if (*slave_adr==3){
		bit_clear(PORTC,BIT(PC2));
		return 1; // operation successful
	}
	else if (*slave_adr==4){
		bit_clear(PORTD,BIT(PD5));
		return 1;
	}
	else if (*slave_adr==5){
		bit_clear(PORTB,BIT(PB6));
		return 1;
	}
	else if (*slave_adr==6){
		bit_clear(PORTB,BIT(PB7));
		return 1;
	}
	else if (*slave_adr==7){
		bit_clear(PORTC,BIT(PC7));
		return 1;
	}
	else if (*slave_adr==8){
		bit_clear(PORTC,BIT(PC6));
		return 1; // operation successful
	}
	// add if for every SS pin
	else
		return 0; // adr not found
} 

void usb_write(const char *str) {
    while (*str) { 
        usb_serial_putchar(*str); 
        str++; 
	}
}

uint8_t recv_str(char *buf, uint8_t size) {
	char data;
	uint8_t count=0;
	
	while (count < size) {
		data = usb_serial_getchar();
		//usb_serial_putchar(data);
		if (data == '\r' || data == '\n') {
			*buf = '\0';
			return size;
		}
		if (data >= ' ' && data <= '~') {
			*buf++ = data;
			count++;
		}		
	}
	return count;
}

void parse_and_execute_command(char *buf) {
	char *command;
	char ord;
	int16_t par1;
	char adr;
	uint8_t start_from = 0;
	command = buf;
	// k�sk saab olema sdx:y kus x on pesa number ja y on kiirus.
	if ((command[0] == 's') && (command[1] == 'd')) {
		// sdx � Liigu PID-ga kiirusega x
		start_from =2;
		uint8_t repeat;
		do {
			repeat = 0;
			adr = command[start_from] - '0';
			par1 = atoi(command+start_from+2);
			ord = ord_LM_sd;
				if (par1<0){
					ord |=1; // direction bit
					par1 *= -1;
				}
			queue_push(ord,adr);
			queue_push(par1,adr);
			sprintf(response, "<%i;%i>",adr,par1);
			usb_write(response);
			if (find_next_arg(command+start_from)!=255){
				start_from+=find_next_arg(command+start_from)+1;
				repeat = 1;
			}
		} while (repeat);
	}
	
	else if ((command[0] == 'i') && (command[1] == 'd')){
		adr = command[2] - '0';
		if ((adr>=1) && (adr<=8)){
			par1 = atoi(command+4);
			if ((par1>=0) && (par1<=7)){
				ord = ord_LM_setID;
				queue_push(ord,adr);
				ord = par1;
				queue_push(ord,adr);
			} 
			else
				usb_write("vigane id\n");
		}
		else
			usb_write("vigane f. aadress\n");
		
	}
	else if (command[0] == '?'){
		
	}
	
	else if ((command[0] == 'w') && (command[1] == 'l')) {
		// wdx � Liigu ilma PID-ta kiirusega x
		par1 = atoi(command+2);
		ord = dev_LM | ord_LM_wl;
		if (par1<0){
			ord |=1; // direction bit
			par1 *= -1;
		}
		sprintf(response,"<%c>\n",ord);
		usb_write(response);
		SPDR = ord;
		while(!(SPSR & (1<<SPIF))){}
		
		_delay_ms(100);
		SPDR = par1;
		while(!(SPSR & (1<<SPIF))){}
			
	}
	else if ((command[0] == 'd') && (command[1] == 'r')) {
		// dr0/1 � Seab ratta p��rlemise suuna
		par1 = atoi(command+2);
		ord = dev_LM | ord_LM_dr;
		if (par1) ord |= 1;
		SPDR = ord;
		while(!(SPSR & (1<<SPIF))){}
	}
	else if ((command[0] == 'p') && (command[1] == 'd')) {
		// pd0/1 � l�litab PID v�lja/sisse
		par1 = atoi(command+2);
		ord = dev_LM | ord_LM_pd;
		if (par1) ord |= 1;
		SPDR = ord;
		while(!(SPSR & (1<<SPIF))){}
	}
	else if ((command[0] == 'g') && (command[1] == 's')) {
		// gs0/1 � l�litab automaatse kiiruse saatmise v�lja/sisse
		par1 = atoi(command+2);
		ord = dev_LM | ord_LM_gs;
		if (par1) ord |= 1;
		SPDR = ord;
		while(!(SPSR & (1<<SPIF))){}
	}
	else if ((command[0] == 's') && (command[1] == 't')) {
		// st � sooritab polaarsustesti
		ord = dev_LM | ord_LM_st;
		SPDR = ord;
		while(!(SPSR & (1<<SPIF))){}
	}
	else if ((command[0] == 'f') && (command[1] == 's')) {
		//toggle failsafe
		par1 = atoi(command+2);
		ord = dev_LM | ord_LM_fs;
		if (par1) ord |= 1;
		SPDR = ord;
		while(!(SPSR & (1<<SPIF))){}		
	}
	else if (command[0] == 's'){
		// S � k�sib ratta kiiruse
		ord = dev_LM | ord_LM_s;
		SPDR = ord;
		while(!(SPSR & (1<<SPIF))){}
		ord = dev_LM | ord_LM_ok;
		SPDR = ord;
		while(!(SPSR & (1<<SPIF))){}
		_delay_ms(1);
		par1=SPDR;
		ord = dev_LM | ord_LM_ok;
		SPDR = ord;
		while(!(SPSR & (1<<SPIF))){}
		_delay_ms(1);
		if (par1&1){
			par1=SPDR;
			par1*=-1;
		}
		else{
			par1=SPDR;
		}
		sprintf(response,"<s:%i",par1);
		usb_write(response);	
	}
	else {
		// Ei saanud k�sust aru, saadan tagasi
		sprintf(response, "ei saa aru:%s\n", command);
		usb_write(response);
	}
}

void spi_handle_response(uint16_t slave_response){
	if (spi_waiting_for_answer[*slave_adr]){ // do we want to hear something from the slave
		if ((slave_response>>8) & 1<<7){ // is it useful info
			if (slave)
		
		}
	}
	bit_flip(PORTC,BIT(LED1));
	// TODO	
}

int main(void) {	
	CLKPR = 0x80;
	CLKPR = 0x00;
	usb_init();
	
	//init leds	
	bit_set(DDRC, BIT(LED1));	
	bit_set(DDRC, BIT(LED2));
	bit_set(PORTC, BIT(LED1));
	bit_set(PORTC, BIT(LED2));	
	
	// INIT SLAVE SELECTS
	//pins for v1 device
	/*DDRB |= BIT(PB0) | BIT(PB4) | BIT(PB5) | BIT(PB6);
	DDRD |= BIT(PD0) | BIT(PD1) | BIT(PD3) | BIT(PD4);
	
	PORTB |= BIT(PB0) | BIT(PB4) | BIT(PB5) | BIT(PB6);
	PORTD |= BIT(PD0) | BIT(PD1) | BIT(PD3) | BIT(PD4);*/
	//pins for v2 device
	DDRB |= BIT(PB0) | BIT(PB4) | BIT(PB6) | BIT(PB7);
	DDRC |= BIT(PC2) | BIT(PC6) | BIT(PC7);
	DDRD |= BIT(PD5);
	
	PORTB |= BIT(PB0) | BIT(PB4) | BIT(PB6) | BIT(PB7);
	PORTC |= BIT(PC2) | BIT(PC6) | BIT(PC7);
	PORTD |= BIT(PD5);
	
	
	//Wait for USB to be configured
	while (!usb_configured()) /* wait */ ;
	_delay_ms(500);
	
	// spi init
	/* Set MOSI and SCK and SS output, all others input */
	DDRB |= (1<<2)|(1<<1)|(1);
	/* Enable SPI, Master, set clock rate fck/16 */
	SPCR = (1<<SPE)|(1<<MSTR)|0b00000011;
	
	// init timer 
	TCCR0B = BIT(CS00) | BIT(CS02);
	TIMSK0 = BIT(TOIE0);
	
	
	
	sei();
	
	uint8_t n;
	char buf[32];
	
	// init complete signal
	bit_clear(PORTC, BIT(LED1));	
	
	while (1) {
			
		if (usb_serial_available()) {
			n = recv_str(buf, sizeof(buf));
			if (n == sizeof(buf)) {
				parse_and_execute_command(buf);
			}
		}
		
		if (!queue_isEmpty()){
			//new data to send!
			do
			{
				spi_handle_response(spi_communicate());
			} while (!queue_isEmpty());
		}
			
	}
}