// Queue header file

#ifndef spi_queue__
#define spi_queue__

#include <avr/io.h>

#define QUEUE_SIZE 64

char queue_puff[QUEUE_SIZE];
char queue_adr[QUEUE_SIZE];
uint8_t first_element;
uint8_t last_element;
char *first_adr; // valid only after queue_pop returns true
char *first_puff;

void queue_init(); // initializes first and last element and first_adr

uint8_t queue_push(char elem, char adr); // returns true or false: true - ewerithing ok, false - queue full

uint8_t queue_pop(); // returns true or false -

uint8_t queue_isEmpty(); 

#endif